FROM python:3.13.0b1-alpine3.19

# 建立工作目錄
WORKDIR /app

RUN apk update && \
    apk add --no-cache \
    build-base \
    mariadb-connector-c-dev \
    && rm -rf /var/cache/apk/*

# 複製當前目錄下的代碼到容器中 /app
COPY . .

# 安裝所需套件
RUN pip install -r requirements.txt

# 設置環境變數
ENV FLASK_APP=app.py

EXPOSE 5001

# 在容器啟動後的執行命令
CMD ["flask", "run", "--host=0.0.0.0","--port=5001"]
